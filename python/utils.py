# system modules
import datetime

# internal modules

# external modules


def system_timezone():
    return datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
