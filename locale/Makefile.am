existing_pofiles = $(wildcard *.po)
existing_mofiles = $(wildcard *.mo)

locales = $(sort $(existing_pofiles:.po=) $(existing_mofiles:.mo=))

qmfiles = $(foreach locale,$(locales),@PACKAGE_NAME@-$(locale).qm)

translationdir = $(datadir)/@PACKAGE_NAME@/translations
translation_DATA = $(qmfiles)

DEdir = $(datadir)/@PACKAGE_NAME@/locale/de/LC_MESSAGES
DE_DATA = locale/de/LC_MESSAGES/@PACKAGE_NAME@.mo

FRdir = $(datadir)/@PACKAGE_NAME@/locale/fr/LC_MESSAGES
FR_DATA = locale/fr/LC_MESSAGES/@PACKAGE_NAME@.mo

EXTRA_DIST = $(DE_DATA) $(existing_pofiles) $(translation_DATA)

qmldir=../qml
qmlsources = $(shell find $(qmldir) -type f -iname '*.qml')
qmltsfile = qml.ts
qmlpotfile = qml.pot

pythondir=../python
pythonsources = $(shell find $(pythondir) -type f -iname '*.py')
pythonpotfile = python.pot
potfile = all.pot

msg_opts = --force-po --sort-output
xgettext_opts = $(msg_opts) --no-location --from-code=UTF-8

define po_merge_rule
locale/$(1)/LC_MESSAGES:
	$(mkdir_bin) -p $$@

locale/$(1)/LC_MESSAGES/@PACKAGE_NAME@.po: $(1).po | locale/$(1)/LC_MESSAGES
	ln -rsf $$< $$@
endef

%.po: $(potfile)
	VERSION_CONTROL=off $(msgmerge_bin) $(msg_opts) -U $@ $< && touch $@

$(potfile): $(pythonpotfile) $(qmlpotfile)
	$(msgcat_bin) $(msg_opts) --unique -o $@ $^


$(pythonpotfile): $(pythonsources)
	$(xgettext_bin) -L Python $(xgettext_opts) -o $@ $^

qml_tr_keywords = qsTr qsTranslate:1c,2
$(qmlpotfile): $(qmlsources)
	$(xgettext_bin) -L JavaScript $(foreach kw,$(qml_tr_keywords),-k$(kw) ) $(xgettext_opts) -o $@ $^

$(foreach locale,$(locales),$(eval $(call po_merge_rule,$(locale))))

%.mo: %.po
	$(msgfmt_bin) -o $@ $< && touch $@

@PACKAGE_NAME@-%.qm: %.po
	$(msgfmt_bin) --qt -o $@ $< && touch $@



clean-local:
	rm -f $(qmltsfile)
	rm -f $(pythonpotfile) $(qmlpotfile) $(potfile)
	rm -f $(existing_mofiles)
