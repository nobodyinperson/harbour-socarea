# Prevent brp-python-bytecompile from running.
%define __os_install_post %{___build_post}

# "Harbour RPM packages should not provide anything."
%define __provides_exclude_from ^%{_datadir}/.*$

Name: harbour-socarea
Version: 0.0.0
Release: jolla
Summary: Application to interface Sony Cameras
License: GPLv3+
URL: https://gitlab.com/nobodyinperson/harbour-socarea
Source: %{name}-%{version}.tar.xz
Packager: Yann Büchau <nobodyinperson@gmx.de>
Conflicts: %{name}
BuildArch: noarch
BuildRequires: gettext
BuildRequires: inkscape
BuildRequires: gettext
BuildRequires: pandoc
BuildRequires: make
Requires: pyotherside-qml-plugin-python3-qt5 >= 1.4
Requires: python3-base
Requires: python3-sonycam >= 0.0.11
Requires: sailfishsilica-qt5

%description
SailfishOS application to interface Sony Cameras featuring the Sony Camera
Remote API

%prep
%setup -q

%install
./configure --prefix=/usr
make
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}.svg
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}/qml/*.qml
%{_datadir}/%{name}/python/*.py
%{_datadir}/%{name}/images/*.svg
%{_datadir}/%{name}/translations/*.qm
%{_datadir}/%{name}/locale/*/LC_MESSAGES/%{name}.mo

%changelog

