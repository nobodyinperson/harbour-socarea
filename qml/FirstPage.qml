import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: firstpage
    property var coverContent: CoverContent {
        text: qsTranslate("cover", "Welcome")
    }

    SilicaFlickable {
        id: view
        anchors.fill: parent

        PullDownMenu {
            busy: !python.imported
            enabled: python.imported

            MenuItem {
                text: qsTranslate("menu","About")
                onClicked: app.pageStack.push("AboutPage.qml")
            }

            ActionsMenuLabel {}
        }

        PageHeader {
            id: header
            width: parent.width
            title: qsTranslate("first-page","Socarea")
        }

        ViewPlaceholder {
            id: viewPlaceholder
            enabled: python.imported
            text: qsTranslate("first-page","Welcome to Socarea!")
            hintText: qsTranslate("first-page", "...")
        }

        BusyIndicator {
            id: busyindicator
            visible: !python.imported
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: !python.imported
        }

        Label {
            id: loadinglabel
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: busyindicator.top
                bottomMargin: Theme.paddingLarge
            }
            font.pixelSize: Theme.fontSizeLarge
            color: Theme.highlightColor
            visible: busyindicator.running
            text: qsTranslate("first-page", "Loading backend")
        }

    }
}


